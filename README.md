Device Tree for Realme 2 Pro
===========================================

The Realme 2 Pro (codenamed _"RMX1801"_) is a mid-range smartphone from Realme.
It was announced in September 2018

## Device specifications

Basic   | Spec Sheet
-------:|:-------------------------
CPU | Octa-core (4x2.2 GHz Kryo 260 Gold Cortex-A73 & 4x1.84 GHz Kryo 260 Silver Cortex-A53)
Chipset | Qualcomm Snapdragon 660 AIE
GPU | Adreno 512
Memory  | 4GB RAM
Shipped Android | 8.1 (oreo)
Storage | 64GB
MicroSD | Up to 256GB
Battery | Non-removable Li-Ion 3500 mAh
Display | 6,3” IPS, 2.5D Gorilla® Glass 3, 2340*1080, FHD+，408 PPI
Camera (Rear) | 16 MP f / 1.7 + 2 MP f / 2.4, LED Flash
Camera (Front) | 16 MP f/2.0

## Device picture

![Realme 2 Pro](https://static.realme.net/page/realme2pro/images/specs-black-sea-0cf1849e61.png "Realme 2 Pro")
